'use strict';
/**
 * Created by seiya on 15/9/6.
 */
var request=require('request');
var wxjsticket=require('../config/wxjsticket');
var mw_checkapiticket=function(app){
    return function(req,res,next){
        var locals=app.locals;
        var wxJSSDKConfig=locals.wxJSSDKConfig;
        var appId=wxjsticket.appid;
        var getTokenUrl=wxjsticket.tokenUrl+'&appid='+appId+'&secret='+wxjsticket.secret;
        var getTicketCB=function(error, response, body){
            if (!error && response.statusCode === 200) {
                body=JSON.parse(body);
                if(body.errcode===0){
                    locals.wxJSSDKConfig={
                        appId:appId,
                        ticket:body.ticket,
                        expires_in:body.expires_in,
                        getTicketTimeStamp:new Date().getTime()//用于服务内计算ticket是否过期
                    };
                }
                next();
            }
        };
        var getTokenCB=function (error, response, body){
            if (!error && response.statusCode === 200) {
                body=JSON.parse(body);
                var access_token=body.access_token;
                if(!access_token){
                    //没有获取access_token成功，直接执行下一步
                    return next();
                }
                var getTicketUrl=wxjsticket.getTicketUrl+'?access_token='+access_token+'&type=jsapi';
                request(getTicketUrl,getTicketCB);
            }
        };
        //ticket不存在
        if(!wxJSSDKConfig){
            request(getTokenUrl,getTokenCB);
        }else{
            //是否过期
            var now=new Date();
            var expired=(now.getTime()-wxJSSDKConfig.getTicketTimeStamp)>wxJSSDKConfig.expires_in*1000;
            if(expired){
                request(getTokenUrl,getTokenCB);
            }else{
                next();
            }
        }
    };
};
module.exports=mw_checkapiticket;