"use strict";
/**
 * Created by seiya on 15/9/6.
 */
var app=require('express')();
var bodyParser=require('body-parser');
var swig=require('swig');
var consolidate=require('consolidate');
app.engine('html', consolidate['swig']);
app.set('view engine', 'html');
app.set('views', __dirname + '/views');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
var checkapiticket=require('./controllers/mw_checkapiticket');
var sign=require('./controllers/sign');
app.get('/api/wxjssdk',checkapiticket(app),function(req,res){
    var locals=app.locals;
    var jsticket=sign(locals.wxJSSDKConfig.ticket,req.query.authorUrl);
    jsticket.appId=locals.wxJSSDKConfig.appId;
    var result={
        result:0,
        wxjssdkConfig:jsticket
    };
    res.json(result);
});
app.get('/',function(req,res){
    res.render('index',{title:'微信JS-SDK能力调用DEMO页'},function(err,html){
        res.send(html);
    });
});
var port=8888;
var host='10.15.1.210';
app.listen(port,host,function(){
    console.log('app is listening '+host+':'+port);
});
